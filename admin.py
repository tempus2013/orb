# -*- coding: utf-8 -*-

from django.contrib import admin
from django import forms
from django.utils.translation import ugettext as _
from django.db import transaction
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse
from django.http import HttpResponse

from django.utils.safestring import mark_safe

from xml.dom import minidom
import csv
import hashlib
from django.core.mail import send_mail, get_connection

from apps.orb import settings

from apps.orb.models import Poll, PollType, ActivePollType, Stage, Sheet, Question, SubQuestion, QuestionAnswer, Condition, ConditionComponent, Respondent, RespondentAnswer

# -------------------------------------------------------------------
# --- POLL
# -------------------------------------------------------------------


class PollAdminForm (forms.ModelForm):
    """
    Custom Poll for in admin.
    """
    class Meta:
        model = Poll

    process_poll_file = forms.BooleanField(required=False, label=_("Overwrite file"), help_text=_("Overwritting file will erase all existing answers"))
    process_respondents_file = forms.BooleanField(required=False, label=_("Overwrite file"), help_text=_("Overwritting file will update respondents list"))


class PollAdmin(admin.ModelAdmin):
    form = PollAdminForm
    list_display = ('title', 'type_column', 'date_from', 'date_to')
    search_fields = ('title',)
    fieldsets = (
                    (None, {'fields': ('title', 'date_from', 'date_to', 'announcement_title', 'announcement_content', 'invitation_title', 'invitation_content', 'reminder_title', 'reminder_content', 'thanks_content')}),
                    (_("Poll file"), {'fields': ('process_poll_file', 'poll_file'), 'classes': ('collapse',),}),
                    (_("Respondents file"), {'fields': ('process_respondents_file', 'respondents_file',), 'classes': ('collapse',),}),
                 )
    
    def save_model(self, request, obj, form, change):
        """
        Overwrites saving model
        """
        obj.save()
        
        process_poll_file = form.cleaned_data['process_poll_file']
        process_respondents_file = form.cleaned_data['process_respondents_file']
        
        # Processing poll XML File
        if process_poll_file:  # Poll's structure can be defined only one time
            try:
                handle_poll_xml_file(obj)
            except:
                obj.poll_file = None
                obj.save()
                raise

        # Processing respondents CSV file
        if process_respondents_file:
            handle_respondents_csv_file(obj)
            self.message_user(request, _('Respondents have been successfully uploaded'))

    def type_column(self, obj):
        """
        Defines poll type column. 
        """
        return obj.activepolltype.type
    type_column.short_description = (u'Type')

@transaction.commit_on_success
def handle_poll_xml_file(poll):
    """
    Saves strucuture of a poll to a database.
    """
    # Removing existing structures
    poll.stage_set.all().delete()
    
    # Paring XML file
    dom = minidom.parse(poll.poll_file)
    
    poll_xml = dom.getElementsByTagName('poll')[0]
    structure_xml = dom.getElementsByTagName('structure')[0]
    conditions_xml = dom.getElementsByTagName('conditions')[0]
    
    # Setting poll's title
    poll.title = poll_xml.getAttribute('title')
    
    # Przetworzenie sekcji ze strukturą
    stage_order = 0
    for stage_xml in structure_xml.getElementsByTagName('stage'):
        stage = Stage()
        stage.poll = poll
        stage.order = stage_order
        stage.save()
        stage_order += 1
        sheet_order = 0
        for sheet_xml in stage_xml.getElementsByTagName('sheet'): 
            sheet = Sheet()
            sheet.node_id = sheet_xml.getAttribute('id')
            sheet.title = sheet_xml.getAttribute('title')
            sheet.stage = stage
            sheet.order = sheet_order
            sheet_info = sheet_xml.getElementsByTagName('info')
            if sheet_info:  # Węzeł z informacjami o arkuszu został zdefiniowany
                sheet.info = sheet_info[0].firstChild.nodeValue
            sheet.save()
            sheet_order += 1
            questions_xml = sheet_xml.getElementsByTagName('questions')
            if questions_xml:  # Blok z pytaniami został zdefiniowany
                for question_xml in questions_xml[0].getElementsByTagName('question'):
                    question = Question()
                    question.node_id = int(question_xml.getAttribute('id'))
                    question.sheet = sheet
                    question.content = question_xml.getElementsByTagName('content')[0].firstChild.nodeValue
                    question.type = int(question_xml.getAttribute('type'))
                    try:
                        question.max_answers = int(question_xml.getAttribute('max_answers'))
                    except:
                        pass
                    question.save()
                    answers_xml = question_xml.getElementsByTagName('answers')
                    subquestions_xml = question_xml.getElementsByTagName('subquestions')
                    
                    if answers_xml:  # Blok z odpowiedziami został zdefiniowany
                        for answer_xml in answers_xml[0].getElementsByTagName('answer'):
                            answer = QuestionAnswer()
                            answer.node_id = answer_xml.getAttribute('id')
                            answer.question = question
                            answer.content = answer_xml.firstChild.nodeValue
                            answer.order = int(answer_xml.getAttribute('order'))
                            answer.save()
                            
                    if subquestions_xml:
                        for subquestion_xml in subquestions_xml[0].getElementsByTagName('subquestion'):
                            subquestion = SubQuestion()
                            subquestion.node_id = subquestion_xml.getAttribute('id')
                            subquestion.question = question
                            subquestion.content = subquestion_xml.firstChild.nodeValue
                            subquestion.save()
        
    # Przetworzenie sekcji z warunkami
    for condition_xml in conditions_xml.getElementsByTagName('condition'):
        condition = Condition()
        condition.poll = poll
        condition.save()
        
        for answer_xml in condition_xml.getElementsByTagName('answer'):
            sheet = Sheet.objects.get(stage__poll=poll, node_id=answer_xml.getAttribute('sheet'))
            question = Question.objects.get(sheet=sheet, node_id=answer_xml.getAttribute('question'))
            answer = QuestionAnswer.objects.get(question=question, node_id=answer_xml.getAttribute('answer'))
            negation = answer_xml.getAttribute('negation')
            
            condition_component = ConditionComponent()
            condition_component.sheet = sheet
            condition_component.question = question
            condition_component.answer = answer
            if negation:
                condition_component.negation = bool(negation)
            condition_component.save()
            
            condition.condition_components.add(condition_component)
        
        for sheet_xml in condition_xml.getElementsByTagName('sheet'):
            sheet = Sheet.objects.get(stage__poll=poll, node_id=sheet_xml.getAttribute('id'))
            condition.sheets.add(sheet)


@transaction.commit_on_success
def handle_respondents_csv_file(poll):
    """
    Saves list of respondents to a database.
    """
    csv_file = open(poll.respondents_file.path)
    csv_rows = csv.reader(csv_file, delimiter=',', quotechar='"')
    for row in csv_rows:
        email = row[0]
        m = hashlib.md5()
        m.update("token%s" % email)
        token = m.hexdigest()[:5]
        vcode = m.hexdigest()[5:10]
        
        respondent = Respondent.objects.get_or_create(poll=poll, email=email, token=token, vcode=vcode)

# -------------------------------------------------------------------
# --- RESPONDENT
# -------------------------------------------------------------------

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

from django.core.mail import EmailMultiAlternatives


class RespondentAdmin(admin.ModelAdmin):
    list_display = ('email', 'poll', 'is_announcement_sent', 'is_invitation_sent', 'is_reminder_sent', 'is_completed', 'last_update')
    list_filter = ('poll',)
    search_fields = ('email',)
    actions = ['erease_answers', 'send_announcements', 'send_invitations', 'send_reminders']

    def create_connection(self):
        connection_kwargs = {}
        connection_kwargs['host'] = settings.QUESTIONNAIRE_EMAIL_HOST
        connection_kwargs['port'] = settings.QUESTIONNAIRE_EMAIL_PORT
        connection_kwargs['username'] = settings.QUESTIONNAIRE_EMAIL_HOST_USER
        connection_kwargs['password'] = settings.QUESTIONNAIRE_EMAIL_HOST_PASSWORD
        connection_kwargs['use_tls'] = settings.QUESTIONNAIRE_EMAIL_USE_TLS
        return get_connection(backend=EMAIL_BACKEND, kwargs=connection_kwargs)

    def erease_answers(self, request, queryset):
        """
        Ereases all respondents' answers
        """
        for respondent in queryset:
            respondent.sheets_completed.clear()
            RespondentAnswer.objects.filter(respondent = respondent).delete()
        self.message_user(request, "Number of successfully ereased respondents: %s." % queryset.count())
    erease_answers.short_description = _("Erease answers")
    
    def send_announcements(self, request, queryset):
        """
        Sends announcements to respondents
        """
        connection = self.create_connection()
        sent = 0
        for respondent in queryset:
            if not respondent.is_announcement_sent:
                subject = respondent.poll.announcement_title
                content = respondent.poll.announcement_content
                try:
                    send_mail(subject, content, settings.QUESTIONNAIRE_DEFAULT_FROM_EMAIL, [respondent.email], fail_silently=False, connection=connection)
                    respondent.is_announcement_sent = True
                    respondent.save()
                    sent += 1
                except:
                    raise
        self.message_user(request, "%s out of %s announcements have been sent." % (sent, queryset.count()))
    send_announcements.short_description = _("Send announcements")

    def send_invitations(self, request, queryset):
        """
        Sends invitations to respondents
        """

        connection = self.create_connection()
        sent = 0
        for respondent in queryset:
            if not respondent.is_invitation_sent and respondent.is_announcement_sent:
                subject = respondent.poll.invitation_title
                content = respondent.poll.invitation_content
                poll_url = reverse("apps.orb.views.poll_show", kwargs={"poll_id": respondent.poll.id, "token": respondent.token})
                content_plain = content.replace("%url%", "http://syjon.umcs.lublin.pl"+poll_url)
                content_html = content.replace("%url%", "<a href='http://syjon.umcs.lublin.pl"+poll_url+"'>Twoja ankieta</a>")
                content_html = content_html.replace("\n", "<br/>")
                try:
                    msg = EmailMultiAlternatives(subject, content_plain, settings.QUESTIONNAIRE_DEFAULT_FROM_EMAIL, [respondent.email])
                    msg.attach_alternative(content_html, "text/html")
                    msg.connection = connection
                    msg.send()

                    sent += 1

                    respondent.is_invitation_sent = True
                    respondent.save()
                except:
                    raise
        self.message_user(request, "%s out of %s invitations have been sent." % (sent, queryset.count()))
    send_invitations.short_description = _("Send invitations")

    def send_reminders(self, request, queryset):
        """
        Sends reminders to respondents
        """

        connection = self.create_connection()
        sent = 0
        for respondent in queryset:
            if not respondent.is_reminder_sent and not respondent.is_completed and respondent.is_invitation_sent:
                subject = respondent.poll.reminder_title
                content = respondent.poll.reminder_content
                poll_url = reverse("apps.orb.views.poll_show", kwargs={"poll_id": respondent.poll.id, "token": respondent.token})
                content_plain = content.replace("%url%", "http://syjon.umcs.lublin.pl"+poll_url)
                content_html = content.replace("%url%", "<a href='http://syjon.umcs.lublin.pl"+poll_url+"'>Twoja ankieta</a>")
                content_html = content_html.replace("\n", "<br/>")
                try:
                    msg = EmailMultiAlternatives(subject, content_plain, settings.QUESTIONNAIRE_DEFAULT_FROM_EMAIL, [respondent.email])
                    msg.attach_alternative(content_html, "text/html")
                    msg.connection = connection
                    msg.send()

                    sent += 1

                    respondent.is_reminder_sent = True
                    respondent.save()
                except:
                    raise
        self.message_user(request, "%s out of %s reminders have been sent." % (sent, queryset.count()))
    send_reminders.short_description = _("Send reminders")

# -------------------------------------------------------------------
# --- SHEET
# -------------------------------------------------------------------


class SheetAdmin(admin.ModelAdmin):
    list_display = ('title', 'stage', 'order', 'get_poll')
    list_filter = ('stage__poll',)
    search_fields = ('title',)

    def get_poll(self, obj):
        return obj.stage.poll
    get_poll.short_description = _(u'Poll')

# -------------------------------------------------------------------
# --- STAGE
# -------------------------------------------------------------------


class StageAdmin(admin.ModelAdmin):
    list_display = ('order', 'poll')
    list_filter = ('poll',)

# -------------------------------------------------------------------
# --- QUESTION
# -------------------------------------------------------------------


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('content', 'sheet', 'type', 'max_answers', 'order', 'get_poll')
    list_filter = ('sheet', 'sheet__stage__poll')
    search_fields = ('content',)

    def get_poll(self, obj):
        return obj.sheet.stage.poll
    get_poll.short_description = _(u'Poll')

# -------------------------------------------------------------------
# --- CONDITION
# -------------------------------------------------------------------


class ConditionAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_condition_components', 'get_sheets', 'poll')
    list_filter = ('poll',)
    search_fields = ('^sheet__title',)

    def get_condition_components(self, obj):
        return '<br/>'.join((unicode(condition_component) for condition_component in obj.condition_components.all()))
    get_condition_components.short_description = _(u'Condition components')
    get_condition_components.allow_tags = True

    def get_sheets(self, obj):
        return '<br/>'.join((unicode(sheet) for sheet in obj.sheets.all()))
    get_sheets.short_description = _(u'Sheets')
    get_sheets.allow_tags = True

# -------------------------------------------------------------------
# --- CONDITION COMPONENT
# -------------------------------------------------------------------


class ConditionComponentAdmin(admin.ModelAdmin):
    list_display = ('sheet', 'question', 'answer', 'get_poll')
    list_filter = ('question__sheet__stage__poll',)

    def get_poll(self, obj):
        return obj.question.sheet.stage.poll
    get_poll.short_description = _(u'Poll')

admin.site.register(Poll, PollAdmin)
admin.site.register(PollType)
admin.site.register(ActivePollType)
admin.site.register(Stage, StageAdmin)
admin.site.register(Sheet, SheetAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Respondent, RespondentAdmin)
admin.site.register(Condition, ConditionAdmin)
admin.site.register(ConditionComponent, ConditionComponentAdmin)