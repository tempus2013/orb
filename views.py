# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from datetime import datetime

from apps.orb.models import Poll, Respondent
from apps.orb.forms import TokenForm, QuestionForm1, QuestionForm2, QuestionForm3, QuestionForm4, QuestionForm5, \
    QuestionForm6, QuestionForm7, QuestionForm8, QuestionForm9, QuestionForm10, QuestionForm11

from apps.orb.models import ActivePollType

TEMPLATE_ROOT = 'orb/'


def index(request):
    active_poll_types = ActivePollType.objects.all()
    kwargs = {'active_poll_types': active_poll_types}
    return render_to_response(TEMPLATE_ROOT+'index.html', kwargs, context_instance=RequestContext(request))


def authentication(request, poll_id):
    poll = get_object_or_404(Poll, id=poll_id)
    if request.GET:
        form = TokenForm(request.GET)
        if form.is_valid():
            token = form.cleaned_data['token']
            try:
                Respondent.objects.get(poll=poll, token=token)
            except Poll.DoesNotExist:
                messages.error(request, _(u"Poll you are trying to open doesn't exist"))
            except Respondent.DoesNotExist:
                messages.error(request, _(u"Token you are trying to use is incorrect"))
            else:
                return redirect(reverse('apps.orb.views.poll_show', kwargs={'poll_id': poll.id, 'token': token}))
        else:
            messages.error(request, _(u"An error occured while processing the form"))
    else:
        form = TokenForm()
    kwargs = {'form': form, 'poll': poll}
    return render_to_response(TEMPLATE_ROOT + 'authentication.html', kwargs, context_instance=RequestContext(request))


def poll_show(request, poll_id, token):

    poll = get_object_or_404(Poll, id=poll_id)
    respondent = get_object_or_404(Respondent, poll=poll, token=token)

    if respondent:      
        sheet = get_current_sheet(poll, respondent)
        if sheet:  # Są jeszcze arkusze do wypełnienia
            forms = create_forms(sheet, request.POST or None)
            if request.POST:  # Formularz został przesłany
                sheet_is_valid = True
                for form in forms:  # Sprawdzenie poprawności przesłanego arkusza
                    if not form.is_valid():
                        sheet_is_valid = False
                        break
                if sheet_is_valid:  # Arkusz przeszedł walidację
                    for form in forms:  # Zapisanie odpowiedzi
                        form.save(respondent)
                    respondent.sheets_completed.add(sheet)  # Przypisanie aktualnego arkusza do listy arkuszy wypełnionych przez respondent
                    respondent.last_update = datetime.now()
                    respondent.save()
                    return redirect(reverse('apps.orb.views.poll_show', kwargs={'poll_id': poll.id, 'token': token}))
                else:  # Arkusz nie przeszedł walidacji
                    messages.error(request, _(u"Correct the following errors before you go to a next sheet"))
        else:  # Wszystkie arkusze zostały wypełnione
            return redirect(reverse('apps.orb.views.poll_completed', kwargs={'poll_id': poll.id, 'token': token}))
        kwargs = {'sheet': sheet, 'forms': forms, 'poll': poll, 'token': token}
        return render_to_response(TEMPLATE_ROOT + 'poll_show.html', kwargs, context_instance=RequestContext(request))
    else:
        messages.error(request, _(u"You don't have permissions to open this page or your session has expired"))
        return redirect(reverse('apps.orb.views.index'))


def poll_completed(request, poll_id, token):
    poll = get_object_or_404(Poll, id=poll_id)
    respondent = get_object_or_404(Respondent, poll=poll, token=token)
    respondent.is_completed = True
    respondent.save()

    message = (poll.thanks_content or '').replace("%vcode%", respondent.vcode).replace("%token%", respondent.token)

    kwargs = {'token': token, 'poll': poll, 'message': message}
    return render_to_response(TEMPLATE_ROOT + 'poll_completed.html', kwargs, context_instance=RequestContext(request))


def get_current_sheet(poll, respondent):
    """
    Zwraca arkusz, który powinien zostać wyświetlony użytkownikowi z danej ankiety.
    """
    for stage in poll.stage_set.all():  # Dla każdego etapu w ankiecie
        for sheet in stage.sheet_set.all():  # Dla każdego arkusza w etapie
            if not sheet.is_filled(respondent):  # Respondent nie wypełnił jeszcze przetwarzanego arkusza
                if sheet.get_conditions():  # Są zdefiniowane warunki wejścia dla aktualnie przetwarzanego arkusza
                    for condition in sheet.get_conditions():  # Dla każdego warunku do aktualnie przetwarzanego arkusza
                        condition_fulfilled = True
                        # Sprawdzenie czy wszystkie odpowiedzi warunku zostały wybrane przez respondenta
                        for condition_component in condition.condition_components.all():  # Dla każdej odpowiedzi w warunku
                            # Jedna z odpowiedzi wymaganych do spełnienia warunku nie została wybrana przez respondenta
                            if not respondent.has_answer(condition_component.answer, condition_component.negation):
                                condition_fulfilled = False
                                break  # Wyjście z pętli sprawdzającej odpowiedzi i przejście do kolejnego warunku
                        if condition_fulfilled:  # Warunek został spełniony.
                            return sheet  # Wyjście z pętli sprawdzającej warunki.
                else:  # Nie zdefiniowanych warunków wejścia do aktualnie przetwarzanego arkusza
                    return sheet
            else:  # Arkusz został już wypełniony
                pass
    return None  # Wszystkie możliwe arkusze zostały wypełnione


def create_forms(sheet, post=None):
    return list(((form_factory(question, post)) for question in sheet.question_set.all()))


def form_factory(question, initial_data):
    if question.type == 1:  # Pytanie otwarte
        return QuestionForm1(initial_data, question=question, prefix=unicode(question.id))
    elif question.type == 2:  # Pytanie jednokrotnego wyboru
        choices = list(((answer.id, answer) for answer in question.questionanswer_set.all()))
        return QuestionForm2(initial_data, choices=choices, question=question, prefix=unicode(question.id))
    elif question.type == 3:  # Pytanie wielokrotnego wyboru
        choices = list(((answer.id, answer) for answer in question.questionanswer_set.all()))
        return QuestionForm3(initial_data, choices=choices, question=question, prefix=unicode(question.id))
    elif question.type == 4:  # Pytanie macierzowe jednokrotnego wyboru
        answers = list(((answer) for answer in question.questionanswer_set.all()))
        subquestions = list(((subquestion) for subquestion in question.subquestion_set.all()))
        return QuestionForm4(initial_data, answers=answers, subquestions=subquestions, question=question, prefix=unicode(question.id))
    elif question.type == 5:  # Pytanie macierzowe jednokrotnego wyboru dla pojedynczej linii
        answers = list(((answer) for answer in question.questionanswer_set.all()))
        subquestions = list(((subquestion) for subquestion in question.subquestion_set.all()))
        return QuestionForm5(initial_data, answers=answers, subquestions=subquestions, question=question, prefix=unicode(question.id))
    elif question.type == 6:  # Pytanie macierzowe wielokrotnego wyboru
        answers = list(((answer) for answer in question.questionanswer_set.all()))
        subquestions = list(((subquestion) for subquestion in question.subquestion_set.all()))
        return QuestionForm6(initial_data, answers=answers, subquestions=subquestions, question=question, prefix=unicode(question.id))
    elif question.type == 7:  # Pytanie jednokrotnego wyboru z polem inne
        choices = list(((answer.id, answer) for answer in question.questionanswer_set.all()))
        return QuestionForm7(initial_data, choices=choices, question=question, prefix=unicode(question.id))
    elif question.type == 8:  # Pytanie wielokrotnego wyboru z polem inne
        choices = list(((answer.id, answer) for answer in question.questionanswer_set.all()))
        return QuestionForm8(initial_data, choices=choices, question=question, prefix=unicode(question.id))
    elif question.type == 9:  # Pytanie datowe z rokiem miesiacem i dniem
        return QuestionForm9(initial_data, question=question, prefix=unicode(question.id))
    elif question.type == 10:  # Pytanie datowe z rokiem i miesiacem
        return QuestionForm10(initial_data, question=question, prefix=unicode(question.id))
    elif question.type == 11:  # Pytanie datowe z rokiem
        return QuestionForm11(initial_data, question=question, prefix=unicode(question.id))