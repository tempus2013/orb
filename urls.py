from django.conf.urls.defaults import patterns

urlpatterns = patterns('',
    (r'^index$', 'apps.orb.views.index'),
    (r'^authentication/(?P<poll_id>\d+)$', 'apps.orb.views.authentication'),
    (r'^poll/(?P<poll_id>\d+)/(?P<token>\w+)$', 'apps.orb.views.poll_show'),
    (r'^poll_completed/(?P<poll_id>\d+)/(?P<token>\w+)$', 'apps.orb.views.poll_completed'),
)