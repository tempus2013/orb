# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Condition.name'
        db.add_column('orb_condition', 'name',
                      self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Condition.name'
        db.delete_column('orb_condition', 'name')


    models = {
        'orb.activepolltype': {
            'Meta': {'object_name': 'ActivePollType', 'db_table': "'orb_active_poll_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'poll': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['orb.Poll']", 'unique': 'True'}),
            'type': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['orb.PollType']", 'unique': 'True'})
        },
        'orb.condition': {
            'Meta': {'object_name': 'Condition'},
            'condition_components': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['orb.ConditionComponent']", 'db_table': "'orb_condition__to__condition_component'", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'sheet': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'condition_sheet'", 'to': "orm['orb.Sheet']"}),
            'sheets': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'condition_sheets'", 'symmetrical': 'False', 'db_table': "'orb_condition__to__sheet'", 'to': "orm['orb.Sheet']"})
        },
        'orb.conditioncomponent': {
            'Meta': {'object_name': 'ConditionComponent', 'db_table': "'orb_condition_component'"},
            'answer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.QuestionAnswer']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Question']"})
        },
        'orb.poll': {
            'Meta': {'object_name': 'Poll'},
            'announcement_content': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'announcement_title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'date_add': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invitation_content': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'invitation_title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'poll_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'respondents_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'orb.polltype': {
            'Meta': {'object_name': 'PollType', 'db_table': "'orb_poll_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'orb.question': {
            'Meta': {'ordering': "('order',)", 'object_name': 'Question'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_answers': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'node_id': ('django.db.models.fields.SmallIntegerField', [], {}),
            'order': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'sheet': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Sheet']"}),
            'type': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        'orb.questionanswer': {
            'Meta': {'ordering': "('order',)", 'object_name': 'QuestionAnswer', 'db_table': "'orb_question_answer'"},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'node_id': ('django.db.models.fields.SmallIntegerField', [], {}),
            'order': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Question']"})
        },
        'orb.respondent': {
            'Meta': {'object_name': 'Respondent'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_announcement_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_invitation_sent': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'poll': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Poll']"}),
            'sheets_completed': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['orb.Sheet']", 'null': 'True', 'db_table': "'orb_respondent__to__sheet'", 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'vcode': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        'orb.respondentanswer': {
            'Meta': {'object_name': 'RespondentAnswer', 'db_table': "'orb_respondent_answer'"},
            'choice': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.QuestionAnswer']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Question']"}),
            'respondent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Respondent']"}),
            'subquestion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.SubQuestion']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'orb.sheet': {
            'Meta': {'ordering': "('stage__order', 'order')", 'object_name': 'Sheet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'node_id': ('django.db.models.fields.SmallIntegerField', [], {}),
            'order': ('django.db.models.fields.SmallIntegerField', [], {}),
            'stage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Stage']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'orb.stage': {
            'Meta': {'ordering': "('order',)", 'object_name': 'Stage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.SmallIntegerField', [], {}),
            'poll': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Poll']"})
        },
        'orb.subquestion': {
            'Meta': {'ordering': "('order',)", 'object_name': 'SubQuestion'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'node_id': ('django.db.models.fields.SmallIntegerField', [], {}),
            'order': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Question']"})
        }
    }

    complete_apps = ['orb']