# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Poll'
        db.create_table('orb_poll', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('date_add', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('date_from', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('date_to', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.PollType'], null=True, blank=True)),
        ))
        db.send_create_signal('orb', ['Poll'])

        # Adding model 'PollType'
        db.create_table('orb_poll_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('orb', ['PollType'])

        # Adding model 'ActivePollType'
        db.create_table('orb_active_poll_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('poll', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['orb.Poll'], unique=True)),
            ('type', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['orb.PollType'], unique=True)),
        ))
        db.send_create_signal('orb', ['ActivePollType'])

        # Adding model 'Stage'
        db.create_table('orb_stage', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('poll', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.Poll'])),
            ('order', self.gf('django.db.models.fields.SmallIntegerField')()),
        ))
        db.send_create_signal('orb', ['Stage'])

        # Adding model 'Sheet'
        db.create_table('orb_sheet', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('node_id', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('stage', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.Stage'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('info', self.gf('django.db.models.fields.TextField')()),
            ('order', self.gf('django.db.models.fields.SmallIntegerField')()),
        ))
        db.send_create_signal('orb', ['Sheet'])

        # Adding model 'Question'
        db.create_table('orb_question', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('node_id', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('sheet', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.Sheet'])),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('type', self.gf('django.db.models.fields.SmallIntegerField')()),
        ))
        db.send_create_signal('orb', ['Question'])

        # Adding model 'SubQuestion'
        db.create_table('orb_subquestion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('node_id', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.Question'])),
        ))
        db.send_create_signal('orb', ['SubQuestion'])

        # Adding model 'QuestionAnswer'
        db.create_table('orb_question_answer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('node_id', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.Question'])),
            ('content', self.gf('django.db.models.fields.TextField')()),
            ('order', self.gf('django.db.models.fields.SmallIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('orb', ['QuestionAnswer'])

        # Adding model 'Condition'
        db.create_table('orb_condition', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sheet', self.gf('django.db.models.fields.related.ForeignKey')(related_name='condition_sheet', to=orm['orb.Sheet'])),
        ))
        db.send_create_signal('orb', ['Condition'])

        # Adding M2M table for field condition_components on 'Condition'
        m2m_table_name = 'orb_condition__to__condition_component'
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('condition', models.ForeignKey(orm['orb.condition'], null=False)),
            ('conditioncomponent', models.ForeignKey(orm['orb.conditioncomponent'], null=False))
        ))
        db.create_unique(m2m_table_name, ['condition_id', 'conditioncomponent_id'])

        # Adding M2M table for field sheets on 'Condition'
        m2m_table_name = 'orb_condition__to__sheet'
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('condition', models.ForeignKey(orm['orb.condition'], null=False)),
            ('sheet', models.ForeignKey(orm['orb.sheet'], null=False))
        ))
        db.create_unique(m2m_table_name, ['condition_id', 'sheet_id'])

        # Adding model 'ConditionComponent'
        db.create_table('orb_condition_component', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.Question'])),
            ('answer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.QuestionAnswer'])),
        ))
        db.send_create_signal('orb', ['ConditionComponent'])

        # Adding model 'Respondent'
        db.create_table('orb_respondent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('poll', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.Poll'])),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('token', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('vcode', self.gf('django.db.models.fields.CharField')(max_length=16)),
        ))
        db.send_create_signal('orb', ['Respondent'])

        # Adding M2M table for field sheets_completed on 'Respondent'
        m2m_table_name = 'orb_respondent__to__sheet'
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('respondent', models.ForeignKey(orm['orb.respondent'], null=False)),
            ('sheet', models.ForeignKey(orm['orb.sheet'], null=False))
        ))
        db.create_unique(m2m_table_name, ['respondent_id', 'sheet_id'])

        # Adding model 'RespondentAnswer'
        db.create_table('orb_respondent_answer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('respondent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.Respondent'])),
            ('choice', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.QuestionAnswer'], null=True, blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.Question'])),
            ('subquestion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['orb.SubQuestion'], null=True, blank=True)),
        ))
        db.send_create_signal('orb', ['RespondentAnswer'])


    def backwards(self, orm):
        # Deleting model 'Poll'
        db.delete_table('orb_poll')

        # Deleting model 'PollType'
        db.delete_table('orb_poll_type')

        # Deleting model 'ActivePollType'
        db.delete_table('orb_active_poll_type')

        # Deleting model 'Stage'
        db.delete_table('orb_stage')

        # Deleting model 'Sheet'
        db.delete_table('orb_sheet')

        # Deleting model 'Question'
        db.delete_table('orb_question')

        # Deleting model 'SubQuestion'
        db.delete_table('orb_subquestion')

        # Deleting model 'QuestionAnswer'
        db.delete_table('orb_question_answer')

        # Deleting model 'Condition'
        db.delete_table('orb_condition')

        # Removing M2M table for field condition_components on 'Condition'
        db.delete_table('orb_condition__to__condition_component')

        # Removing M2M table for field sheets on 'Condition'
        db.delete_table('orb_condition__to__sheet')

        # Deleting model 'ConditionComponent'
        db.delete_table('orb_condition_component')

        # Deleting model 'Respondent'
        db.delete_table('orb_respondent')

        # Removing M2M table for field sheets_completed on 'Respondent'
        db.delete_table('orb_respondent__to__sheet')

        # Deleting model 'RespondentAnswer'
        db.delete_table('orb_respondent_answer')


    models = {
        'orb.activepolltype': {
            'Meta': {'object_name': 'ActivePollType', 'db_table': "'orb_active_poll_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'poll': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['orb.Poll']", 'unique': 'True'}),
            'type': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['orb.PollType']", 'unique': 'True'})
        },
        'orb.condition': {
            'Meta': {'object_name': 'Condition'},
            'condition_components': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['orb.ConditionComponent']", 'db_table': "'orb_condition__to__condition_component'", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sheet': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'condition_sheet'", 'to': "orm['orb.Sheet']"}),
            'sheets': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'condition_sheets'", 'symmetrical': 'False', 'db_table': "'orb_condition__to__sheet'", 'to': "orm['orb.Sheet']"})
        },
        'orb.conditioncomponent': {
            'Meta': {'object_name': 'ConditionComponent', 'db_table': "'orb_condition_component'"},
            'answer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.QuestionAnswer']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Question']"})
        },
        'orb.poll': {
            'Meta': {'object_name': 'Poll'},
            'date_add': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_from': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.PollType']", 'null': 'True', 'blank': 'True'})
        },
        'orb.polltype': {
            'Meta': {'object_name': 'PollType', 'db_table': "'orb_poll_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'orb.question': {
            'Meta': {'object_name': 'Question'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'node_id': ('django.db.models.fields.SmallIntegerField', [], {}),
            'sheet': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Sheet']"}),
            'type': ('django.db.models.fields.SmallIntegerField', [], {})
        },
        'orb.questionanswer': {
            'Meta': {'object_name': 'QuestionAnswer', 'db_table': "'orb_question_answer'"},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'node_id': ('django.db.models.fields.SmallIntegerField', [], {}),
            'order': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Question']"})
        },
        'orb.respondent': {
            'Meta': {'object_name': 'Respondent'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'poll': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Poll']"}),
            'sheets_completed': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['orb.Sheet']", 'db_table': "'orb_respondent__to__sheet'", 'symmetrical': 'False'}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'vcode': ('django.db.models.fields.CharField', [], {'max_length': '16'})
        },
        'orb.respondentanswer': {
            'Meta': {'object_name': 'RespondentAnswer', 'db_table': "'orb_respondent_answer'"},
            'choice': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.QuestionAnswer']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Question']"}),
            'respondent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Respondent']"}),
            'subquestion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.SubQuestion']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        'orb.sheet': {
            'Meta': {'object_name': 'Sheet'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.TextField', [], {}),
            'node_id': ('django.db.models.fields.SmallIntegerField', [], {}),
            'order': ('django.db.models.fields.SmallIntegerField', [], {}),
            'stage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Stage']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'orb.stage': {
            'Meta': {'object_name': 'Stage'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.SmallIntegerField', [], {}),
            'poll': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Poll']"})
        },
        'orb.subquestion': {
            'Meta': {'object_name': 'SubQuestion'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'node_id': ('django.db.models.fields.SmallIntegerField', [], {}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['orb.Question']"})
        }
    }

    complete_apps = ['orb']