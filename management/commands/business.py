# -*- coding: utf-8 -*-
'''
Created on 06-02-2014

@author: pwierzgala

Generuje plik csv z liczebnosciami respondentow
z poszczegolnych kierunków, którzy załozyli firme.
'''

from django.core.management.base import BaseCommand
from django.utils import translation
import syjon
from django.conf import settings

import csv

from apps.orb.models import Respondent
from apps.morpheus.models import GraduateDeclaration
from apps.merovingian.models import Course


def get_department(course):
    department = course.department
    if department:
        if department.type and department.type.id == 1:
            return department

        while department.id != 1:
            if department.type and department.type.id == 1:
                return department
            department = department.department
            if not department:
                return None
    return None


class Command(BaseCommand):

    def handle(self, *args, **options):
        """
        Returns CSV file with processed poll results.
        """

        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))

        questions_ids = [438, 388, 535, 585]
        question_answer_node_id = 1

        respondents = Respondent.objects.filter(respondentanswer__question__id__in=questions_ids, respondentanswer__choice__node_id=question_answer_node_id, is_completed=True)

        respondents_2014 = Respondent.objects.filter(is_completed=True, last_update__year=2014).count()
        respondents_2015 = Respondent.objects.filter(is_completed=True, last_update__year=2015).count()

        respondents_with_business_2014 = respondents.filter(last_update__year=2014).count()
        respondents_with_business_2015 = respondents.filter(last_update__year=2015).count()

        f = open('business.csv', "wb")
        writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        for respondent in respondents:
            declaration = GraduateDeclaration.objects.filter(email=respondent.email).order_by('email').distinct('email')[0]

            department = "???"
            try:
                courses = Course.objects.filter(name=declaration.course_name)
                if courses:
                    department = get_department(courses[0])
            except:
                pass

            respondents, respondents_with_business = '', ''
            if respondent.last_update.year == 2014:
                respondents,respondents_with_business = respondents_2014, respondents_with_business_2014
            else:
                respondents,respondents_with_business = respondents_2015, respondents_with_business_2015
            writer.writerow(
                [
                    unicode(declaration.course_name).encode('utf-8'),
                    unicode(department).encode('utf-8'),
                    respondents,
                    respondents_with_business,
                    respondent.last_update.year
                ]
            )
