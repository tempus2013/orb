# -*- coding: utf-8 -*-
'''
Created on 06-02-2014

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import translation
import syjon
from django.conf import settings

from apps.orb.models import Poll, Question, QuestionAnswer, Respondent, RespondentAnswer
from apps.morpheus.models import GraduateDeclaration
from apps.merovingian.models import CourseLevel

import csv


class Command(BaseCommand):

    poll_id = 4

    def handle(self, *args, **options):
        """
        Returns CSV file with processed poll results.
        """

        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))

        ifile = open('questions.csv', "wb")
        writer = csv.writer(ifile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

        poll = Poll.objects.get(id=self.poll_id)
        questions = Question.objects.filter(sheet__stage__poll=poll)

        for question in questions:
            print question.id

            answers = QuestionAnswer.objects.filter(question=question)

            writer.writerow([question.id, unicode(question.content).encode('utf-8')])
            for answer in answers:
                writer.writerow([answer.id, unicode(answer.content).encode('utf-8')])

            writer.writerow([])

