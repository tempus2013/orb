# -*- coding: utf-8 -*-
'''
Created on 06-02-2014

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from django.core.management.base import CommandError
from django.utils import translation
import syjon
from django.conf import settings

from apps.orb.models import Poll, Question, QuestionAnswer, Respondent, RespondentAnswer
from apps.morpheus.models import GraduateDeclaration
from apps.merovingian.models import CourseLevel, Course
from apps.trainman.models import Department

import csv


class Command(BaseCommand):

    poll_id = 1

    def handle(self, *args, **options):
        """
        Returns CSV file with processed poll results.
        """

        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))

        ifile = open('test.csv', "wb")
        writer = csv.writer(ifile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

        poll = Poll.objects.get(id=self.poll_id)
        respondents = Respondent.objects.filter(poll=poll, is_completed=True)
        print respondents.count()
        questions = Question.objects.filter(sheet__stage__poll=poll)

        row = self.append_header(questions, writer)
        writer.writerow(row)

        for respondent in respondents:
            #print respondent
            row = []

            # Dodanie ID respondenta
            row.append(respondent.id)

            # Dodanie kierunku
            try:
                graduate_declaration = GraduateDeclaration.objects.filter(email=respondent.email)[0]

                if graduate_declaration.course_name == "0":
                    continue

                row.append(unicode(graduate_declaration.course_name).encode('utf-8'))
            except GraduateDeclaration.DoesNotExist:
                print '!!!'
                continue
            except GraduateDeclaration.MultipleObjectsReturned:
                continue
            except IndexError:
                continue

            # Dodanie stopnia kierunku
            course_level = CourseLevel.objects.get(id=graduate_declaration.education_level_id)
            row.append(course_level)

            # Dodanie wydziału
            department = '-'
            try:
                course = Course.objects.filter(name=graduate_declaration.course_name)[0]
                department = self.get_department(course)
            except IndexError:
                department = self.get_department(graduate_declaration.course_name)

            row.append(department)

            for question in questions:
                respondent_answers = RespondentAnswer.objects.filter(question=question, respondent=respondent)  # Pobranie wszystkich odpowiedzi respondenta
                self.append_answers(question, respondent_answers, row)
            writer.writerow(row)

    def append_header(self, questions, file):
        row = []
        row.append('ID respondenta')  # Kolumna z identyfikatorem respondenta
        row.append('Kierunek')  # Kolumna z kierunkiem respondenta
        row.append('Stopień kierunku')  # Kolumna z poziomem kierunku
        row.append('Wydział')  # Kolumna z wydziałem kierunku

        for question in questions:
            if question.type == 2:  # Pytanie jednokrotnego wyboru
                row.append("%s" % question.id)
            elif question.type == 3:  # Pytanie wielokrotnego wyboru
                answers = QuestionAnswer.objects.filter(question=question).count()
                if not question.max_answers is None:
                    answers = question.max_answers
                row.extend(["%s.%s" % (question.id, i+1) for i in range(answers)])
            else:
                row.append('error')
        return row

    def append_answers(self, question, respondent_answers, row):
        if question.type == 2:  # Pytanie jednokrotnego wyboru
            if len(respondent_answers) == 0:  # Respondent nie udzielił odpowiedzi na to pytanie
                self.append_empty_cell(row, 1)
            else:
                row.append(respondent_answers[0].choice.id)
        elif question.type == 3:  # Pytanie wielokrotnego wyboru
            answers_count = QuestionAnswer.objects.filter(question=question).count()  # Pobranie odpowiedzi do pytania
            if not question.max_answers is None:  # Pytanie ma zdefiniowaną maksymalną liczbę odpowiedzi
                answers_count = question.max_answers

            if len(respondent_answers) == 0:  # Respondent nie udzielił odpowiedzi na to pytanie
                self.append_empty_cell(row, answers_count)
            else:
                for respondent_answer in respondent_answers:
                    row.append(respondent_answer.choice.id)

                respondent_answers_count = respondent_answers.count()
                d_answers = answers_count - respondent_answers_count
                if d_answers > 0:  # Respondent udzielił mniej odpowiedzi niż maksimum
                    self.append_empty_cell(row, d_answers)
        else:
            row.append('error')

    def append_empty_cell(self, row, number):
        for i in range(number):
            row.append('')

    def get_department(self, course):
        """
        Zwraca wydział kierunku lub None jeżeli nie udało się ustalić wydziału.
        """
        print course, u'Matematyka', course == u'Matematyka'
        if type(course) == unicode:
            if course == u'Matematyka' or course == u'Matematyka z informatyką':
                return Department.objects.get(id=2)
            elif course == u'Edukacja artystyczna w zakresie sztuk plasycznych':
                return Department.objects.get(id=25)
            elif course == u'Filologia romańska':
                return Department.objects.get(id=8)
            return '-'
        else:
            department = course.department
            if department:
                if department.type and department.type.id == 1:
                    return department

                while department.id != 1:
                    if department.type and department.type.id == 1:
                        return department
                    department = department.department
                    if not department:
                        return None
            return '-'
