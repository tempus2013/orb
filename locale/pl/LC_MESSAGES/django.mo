��    1      �  C   ,      8     9     J  *   \     �  	   �     �     �  
   �  :   �       	   #     -     5     D     V     i  1   x  .   �     �  	   �     �  	   �  
     )        7     =     F     V  	   g  
   q     |     �     �     �     �  +   �               %     +     2     8     ?     K     X  (   ^  #   �  H   �  �  �     �	     �	  2   �	     
     "
     *
     >
     S
  ?   [
     �
     �
     �
     �
     �
     �
     �
  K   �
  >   H     �     �     �     �     �  1   �     �       	   	  
          
   &     1     I     b     n  !   �  %   �     �     �     �     �             
     
        !  2   (  ,   [  H   �                           (          .   +                                     #              0          )   $   &       
   1                         '                          %   	             ,      -   *              !      "   /       Active poll type Active poll types An error occured while processing the form Announcement content Condition Condition component Condition components Conditions Correct the following errors before you go to a next sheet Creation date Date from Date to Erease answers Get results (CSV) Invitation content Overwrite file Overwritting file will erase all existing answers Overwritting file will update respondents list Poll Poll file Poll file (XML) Poll type Poll types Poll you are trying to open doesn't exist Polls Question Question answer Question answers Questions Respondent Respondent answer Respondent answers Respondents Respondents file Respondents file (CSV) Respondents have been successfully uploaded Send announcements Send invitations Sheet Sheets Stage Stages Subquestion Subquestions Title Token you are trying to use is incorrect Type %url% to add personal poll url You don't have permissions to open this page or your session has expired Project-Id-Version: Syjon
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-09-22 16:35+0200
PO-Revision-Date: 2013-09-22 16:36+0100
Last-Translator: Piotr Wierzgała <piotr.wierzgala@umcs.lublin.pl>
Language-Team: Syjon <syjon@umcs.lublin.pl>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Aktywna ankieta typu Aktywne ankiety typów Wystąpił błąd podczas przetwarzania formularza Treść ogłoszenia Warunek Warunek - komponent Warunki - komponenty Warunki Popraw poniższe błędy zanim przejdziesz do kolejnego arkusza Data utworzenia Od Do Wyczyść odpowiedzi Pobierz wyniki (CSV) Treść zaproszenia Nadpisz plik Nadpisanie pliku spowoduje usunięcie wszystkich dotychczasowych odpowiedzi Nadpisanie pliku spowoduje zaktualizowanie listy respondentów Ankieta Plik ankiety Plik ankiety (XML) Typ ankiety Typy ankiet Ankieta, która próbujesz otworzyć nie istnieje Ankiety Pytanie Opowiedź Odpowiedzi Pytania Respondent Respondent - odpowiedź Respondenci - odpowiedzi Respondenci Plik respondentów Plik z listą respondentów (CSV) Respondenci zostali pomyślnie dodani Wyślij ogłoszenie Wyślij zaproszania Arkusz Arkusze Etap Etapy Podpytanie Podpytania Tytuł Token, którego próbujesz użyć jest niepoprawny Wpisz %url% żeby wstawić adres url ankiety Nie masz uprawnień do przeglądanie tej strony lub Twoja sesja wygasła 