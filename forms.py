# -*- coding: utf-8 -*-

import datetime

from django import forms
from django.forms.widgets import CheckboxInput
from itertools import chain
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.utils.encoding import force_unicode
from django.core.validators import MinValueValidator, MaxValueValidator

from apps.orb.models import RespondentAnswer, QuestionAnswer, SubQuestion

# ---------------------------------------------------------------------
# --- KLASA BAZOWA DLA FORMULARZY
# ---------------------------------------------------------------------


class QuestionForm(forms.Form):
    
    question = None  # Obiekt pytania
    
    def __init__(self, *args, **kwargs):
        self.question = kwargs.pop('question')
        super(QuestionForm, self).__init__(*args, **kwargs)
    
    def save(self, respondent):
        pass

# ---------------------------------------------------------------------
# --- KLASA BAZOWA DLA FORMULARZY MACIERZOWYCH
# ---------------------------------------------------------------------


class MatrixForm(QuestionForm):
    
    subquestions = None
    answers = None
    
    def __init__(self, *args, **kwargs):
        self.subquestions = kwargs.pop('subquestions')
        self.answers = kwargs.pop('answers')
        super(MatrixForm, self).__init__(*args, **kwargs)
                
    def choosed_value__to__subquestion_answer_tuple(self, value):
        """
        Zamienia ciąg znaków sformatowany według schematu SubquestionID_QuestionAnswerID na tuple obiektów (SubQuestion, QuestionAnswer)
        """
        (id_subquestion, id_question_answer) = value.split('_')
        question_answer = QuestionAnswer.objects.get(pk=id_question_answer)
        subquestion = SubQuestion.objects.get(pk=id_subquestion)
        return (subquestion, question_answer)


def get_row_css(row, rows, columns):
    """
    Tworzy listę klas jakie należy przypisać do wiersza tabeli.
    """
    css_class = []
    css_class.append('odd' if row % 2 else 'even')
    css_class.append('first_row' if row == 0 else '')
    css_class.append('last_row' if row == rows-1 else '')
    return ' '.join(css_class)    

# ---------------------------------------------------------------------
# --- PYTANIE OTWARTE
# ---------------------------------------------------------------------


class QuestionForm1(QuestionForm):
    """
    Pytanie otwarte.
    """
    text = forms.CharField(widget=forms.Textarea)
    
    def save(self, respondent):
        super(QuestionForm1, self).save(respondent)
        respondent_answer = RespondentAnswer(respondent=respondent, question=self.question, text=self.cleaned_data['text'])
        respondent_answer.save()
        
    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += '%s' % self['text'].errors
        output += '%s' % self['text']
        return mark_safe(output)

# ---------------------------------------------------------------------
# --- PYTANIE JEDNOKROTNEGO WYBORU
# ---------------------------------------------------------------------


class QuestionForm2(QuestionForm):
    """
    Pytanie jednokrotnego wyboru.
    """
    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('choices')
        super(QuestionForm2, self).__init__(*args, **kwargs)
        self.fields['choices'] = forms.ChoiceField(choices=choices, widget=forms.RadioSelect)
        
    def save(self, respondent):       
        try:
            super(QuestionForm2, self).save(respondent)
            question_answer = QuestionAnswer.objects.get(pk=self.cleaned_data['choices'])
            respondent_answer = RespondentAnswer(respondent=respondent, question=self.question, choice=question_answer)
            respondent_answer.save()
        except:
            print 'Wystapił błąd podczas zapisywania formularza 2'
    
    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += u'%s' % self['choices'].errors
        output += u'%s' % self['choices']
        return mark_safe(output)

# ---------------------------------------------------------------------
# --- PYTANIE WIELOKROTNEGO WYBORU
# ---------------------------------------------------------------------


class QuestionForm3(QuestionForm):
    """
    Pytanie wielokrotnego wyboru.
    """
    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('choices')
        super(QuestionForm3, self).__init__(*args, **kwargs)
        self.fields['choices'] = forms.MultipleChoiceField(choices=choices, widget=forms.widgets.CheckboxSelectMultiple)

    def clean_choices(self):
        data = self.cleaned_data['choices']
        if self.question.max_answers and len(data) > self.question.max_answers:
            raise forms.ValidationError("Maksymalna liczba odpowiedzi to %s" % self.question.max_answers)
        return data

    def save(self, respondent):
        try:
            super(QuestionForm3, self).save(respondent)
            for id_question_answer in self.cleaned_data['choices']:
                question_answer = QuestionAnswer.objects.get(pk=id_question_answer)
                respondent_answer = RespondentAnswer(respondent=respondent, question=self.question, choice=question_answer)
                respondent_answer.save()
        except:
            print 'Wystapił błąd podczas zapisywania formularza 3'
            
    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += u'%s' % self['choices'].errors
        output += u'%s' % self['choices']
        return mark_safe(output)

# ---------------------------------------------------------------------
# --- PYTANIE MACIERZOWE JEDNOKROTNEGO WYBORU
# ---------------------------------------------------------------------


class MultiRowSingleChoiceRadioRenderer(forms.widgets.RadioFieldRenderer):
    """
    Generuje wiersz tabeli z etykietą pola w pierwszej komórce i radio buttonami w pozostałych"
    """
    
    def __init__(self, name, value, attrs, choices):
        super(MultiRowSingleChoiceRadioRenderer, self).__init__(name, value, attrs, choices)
    
    def render(self, subquestions, answers):
        output = u''
        rows = len(subquestions)
        columns = len(answers)
        for i, w in enumerate(self):
            if i % columns == 0:
                if i != 0:
                    output += u'</tr>'
                row = i/columns
                css_class = get_row_css(row, rows, columns)
                output += u'<tr class="%s"><td class="subquestion">%s</td>' % (css_class, unicode(subquestions[row]).encode('utf-8'))
            output += u'<td class="input">%s</td>' % w
        return mark_safe(output)


class MultiRowSingleChoiceRadioSelect(forms.widgets.RadioSelect):
    """
    Rozszerza RadioSelect o możliwość przekazania własnego renderera oraz etykiety pola.
    """

    def __init__(self, *args, **kwargs):
        renderer = kwargs.pop('renderer', None)
        self.subquestions = kwargs.pop('subquestions', None)
        self.answers = kwargs.pop('answers', None)
        if renderer:
            self.renderer = renderer
        super(MultiRowSingleChoiceRadioSelect, self).__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None, choices=()):
        return self.get_renderer(name, value, attrs, choices).render(self.subquestions, self.answers)


class QuestionForm4(MatrixForm):
    """
    Pytanie macierzowe jednokrtonego wyboru.
    """
    def __init__(self, *args, **kwargs):
        super(QuestionForm4, self).__init__(*args, **kwargs)
        # Dynamiczne dodanie pól do formularza
        renderer = MultiRowSingleChoiceRadioRenderer
        choices = []
        for subquestion in self.subquestions:
            choices += (('%s_%s' % (subquestion.id, answer.id), '') for answer in self.answers) # Stworzenie tupli odpowiedzi (id, nazwa)
        field = forms.ChoiceField(choices=choices, widget=MultiRowSingleChoiceRadioSelect(renderer=renderer, subquestions=self.subquestions, answers=self.answers), required=True)
        self.fields['choices'] = field 
        
    def save(self, respondent):
        super(QuestionForm4, self).save(respondent)
        (subquestion, question_answer) = self.choosed_value__to__subquestion_answer_tuple(self.cleaned_data['choices'])
        respondent_answer = RespondentAnswer(respondent=respondent, question=self.question, subquestion=subquestion, choice=question_answer)
        respondent_answer.save()
    
    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += '%s' % self['choices'].errors
        output += '<table>'
        output += '<thead>'
        output += '<tr>'
        output += '<td class="corner"></td>'
        for answer in self.answers:
            output += '<td class="answer">%s</td>' % answer
        output += '</tr>'
        output += '</thead>'
        output += '<tbody>'
        output += '%s' % self['choices']
        output += '</tbody>'
        output += '</table>'                     
        return mark_safe(output)

# ---------------------------------------------------------------------
# --- PYTANIE MACIERZOWE JEDNOKROTNEGO WYBORU DLA POJEDYNCZEJ LINII
# ---------------------------------------------------------------------

    
class SingleRowSingleChoiceRadioRenderer(forms.widgets.RadioFieldRenderer):
    """
    Generuje wiersz tabeli z etykietą pola w pierwszej komórce i radio buttonami w pozostałych"
    """
        
    def __init__(self, name, value, attrs, choices):
        super(SingleRowSingleChoiceRadioRenderer, self).__init__(name, value, attrs, choices)

    def render(self, label, row, rows, columns):
        css_class = get_row_css(row, rows, columns)
        return mark_safe(u'<tr class="%s"><td class="subquestion">%s</td>%s</tr>' % (css_class, label, u'\n'.join([u'<td class="input">%s</td>' % w for w in self])))


class SingleRowSingleChoiceRadioSelect(forms.widgets.RadioSelect):
    """
    Rozszerza RadioSelect o możliwość przekazania własnego renderera oraz etykiety pola.
    """

    def __init__(self, *args, **kwargs):
        renderer = kwargs.pop('renderer', None)
        self.label = kwargs.pop('label', None)
        self.row = kwargs.pop('row', 0)
        self.rows = kwargs.pop('rows', 0)
        self.columns = kwargs.pop('columns', 0)
        if renderer:
            self.renderer = renderer
        super(SingleRowSingleChoiceRadioSelect, self).__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None, choices=()):
        return self.get_renderer(name, value, attrs, choices).render(self.label, self.row, self.rows, self.columns)


class QuestionForm5(MatrixForm):
    """
    Pytanie macierzowe jednokrtonego wyboru dla pojedynczej linii.
    """
    def __init__(self, *args, **kwargs):
        super(QuestionForm5, self).__init__(*args, **kwargs)
        # Dynamiczne dodanie pól do formularza
        renderer = SingleRowSingleChoiceRadioRenderer
        for row, subquestion in enumerate(self.subquestions):
            choices = (('%s_%s' % (subquestion.id, answer.id), '') for answer in self.answers) # Stworzenie tupli odpowiedzi (id, nazwa)
            field = forms.ChoiceField(choices=choices, widget=SingleRowSingleChoiceRadioSelect(renderer=renderer, label=subquestion, row=row, rows=len(self.subquestions), columns=len(self.answers)))
            self.fields['subquestion_%s' % subquestion.id] = field
            
    def save(self, respondent):
        super(QuestionForm5, self).save(respondent)
        for name, value in self.cleaned_data.items():
            (subquestion, question_answer) = self.choosed_value__to__subquestion_answer_tuple(value)
            respondent_answer = RespondentAnswer(respondent=respondent, question=self.question, subquestion=subquestion, choice=question_answer)
            respondent_answer.save()
    
    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += '<table>'
        output += '<thead>'
        output += '<tr>'
        output += '<td class="corner"></td>'
        for answer in self.answers:
            output += '<td class="answer">%s</td>' % answer
        output += '</tr>'
        output += '</thead>'
        output += '<tbody>'
        for field in self:
            if field.errors:
                output += '<tr class="errors"><td colspan="%s">%s</td></tr>' % (len(self.answers)+1, field.errors)
            output += '%s' % field
        output += '</tbody>'
        output += '</table>'                     
        return mark_safe(output)
    
# ---------------------------------------------------------------------
# --- PYTANIE MACIERZOWE WIELOKRTONEGO WYBORU
# ---------------------------------------------------------------------    


class CustomCheckboxSelectMultiple(forms.widgets.CheckboxSelectMultiple):
    """
    Otrzymuje listę podpytań i odpowiedzi.
    Na ich podstawie dzieli checboxy na wiersze i przed każdym wyświetla podpytanie.
    """
    
    def __init__(self, *args, **kwargs):
        self.subquestions = kwargs.pop('subquestions', None)
        self.answers = kwargs.pop('answers', None)
        super(CustomCheckboxSelectMultiple, self).__init__(*args, **kwargs)
        
    def render(self, name, value, attrs=None, choices=()):
        if value is None: value = []
        has_id = attrs and 'id' in attrs
        final_attrs = self.build_attrs(attrs, name=name)
        output = []
        
        columns = len(self.answers)
        rows = len(self.subquestions)
        
        # Normalize to strings
        str_values = set([force_unicode(v) for v in value])
        for i, (option_value, option_label) in enumerate(chain(self.choices, choices)):
            # Podział checboxów na wiersze
            if i % columns == 0: # Należy wygenerować kolejny wiersz
                if i != 0: # Wyświetlany jest co najmniej drugi wiersz
                    output.append(u'</tr>')
                
                row = i/columns
                css_class = get_row_css(row, rows, columns)
                output.append(u'<tr class="%s"><td class="subquestion">%s</td>' % (css_class, self.subquestions[row]))
            
            # If an ID attribute was given, add a numeric index as a suffix,
            # so that the checkboxes don't all have the same ID attribute.
            if has_id:
                final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))
                label_for = u' for="%s"' % final_attrs['id']
            else:
                label_for = ''

            cb = CheckboxInput(final_attrs, check_test=lambda value: value in str_values)
            option_value = force_unicode(option_value)
            rendered_cb = cb.render(name, option_value)
            option_label = conditional_escape(force_unicode(option_label))
            output.append(u'<td class="input"><label%s>%s %s</label></td>' % (label_for, rendered_cb, option_label))
        return mark_safe(u'\n'.join(output))


class QuestionForm6(MatrixForm):
    """
    Pytanie macierzowe wielokrotnego wyboru.
    """
    def __init__(self, *args, **kwargs):
        super(QuestionForm6, self).__init__(*args, **kwargs)
        # Dynamiczne dodanie pól do formularza
        choices = []
        for subquestion in self.subquestions:
            choices += (('%s_%s' % (subquestion.id, answer.id), '') for answer in self.answers)  # Stworzenie tupli odpowiedzi (id, nazwa)
        field = forms.MultipleChoiceField(choices=choices, widget=CustomCheckboxSelectMultiple(subquestions=self.subquestions, answers=self.answers))
        self.fields['choices'] = field
    
    def save(self, respondent):
        super(QuestionForm6, self).save(respondent)
        for value in self.cleaned_data['choices']:
            (subquestion, question_answer) = self.choosed_value__to__subquestion_answer_tuple(value)
            respondent_answer = RespondentAnswer(respondent=respondent, question=self.question, subquestion=subquestion, choice=question_answer)
            respondent_answer.save()
            
    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += '%s' % self['choices'].errors
        output += '<table>'
        output += '<thead>'
        output += '<tr>'
        output += '<td class="corner"></td>'
        for answer in self.answers:
            output += '<td class="answer">%s</td>' % answer
        output += '</tr>'
        output += '</thead>'
        output += '<tbody>'
        output += '%s' % self['choices']
        output += '</tbody>'
        output += '</table>'                     
        return mark_safe(output)

# ---------------------------------------------------------------------
# --- PYTANIE JEDNOKROTNEGO WYBORU Z POLEM INNE
# ---------------------------------------------------------------------


class RadioSelectWithOtherRenderer(forms.RadioSelect.renderer):
    """
    RadioFieldRenderer, który wyświetla swój ostatni wybór wraz z polem tekstowym.
    """
    def __init__(self, *args, **kwargs):
        # self.choices przechowuje wszystkie radio buttony poza ostatnim
        # self.other przechowuje ostatni radio button
        super(RadioSelectWithOtherRenderer, self).__init__(*args, **kwargs)
        self.choices, self.other = self.choices[:-1], self.choices[-1]

    def __iter__(self):
        # Wygenerowanie wszystkich radio buttonów oprócz ostatniego. 
        for input in super(RadioSelectWithOtherRenderer, self).__iter__():
            yield input
        
        # Wygenerowanie ostatniego radio buttona.
        if 'id' in self.attrs:
            id = '%s_%s' % (self.attrs['id'], self.other[0]) 
        else:
            id = ''
        
        if id:
            label_for = ' for="%s"' % id
        else:
            label_for = ''
        
        if not force_unicode(self.other[0]) == self.value:
            checked = ''
        else:
            checked = 'checked="true" '
            
        yield '<label %s class="other"><input type="radio" id="%s" value="%s" name="%s" %s/> %s</label>%%s' % (label_for, id, self.other[0], self.name, checked, self.other[1])


class RadioSelectWithOtherWidget(forms.MultiWidget):
    """MultiWidget for use with ChoiceWithOtherField."""
    def __init__(self, choices):
        widgets = [
            forms.RadioSelect(choices=choices, renderer=RadioSelectWithOtherRenderer),
            forms.TextInput
        ]
        super(RadioSelectWithOtherWidget, self).__init__(widgets)

    def decompress(self, value):
        if not value:
            return [None, None]
        return value

    def format_output(self, rendered_widgets):
        """Format the output by substituting the "other" choice into the first widget."""
        return rendered_widgets[0] % rendered_widgets[1]


class RadioSelectWithOtherField(forms.MultiValueField):
    """
    ChoiceField with an option for a user-submitted "other" value.

    The last item in the choices array passed to __init__ is expected to be a choice for "other". This field's
    cleaned data is a tuple consisting of the choice the user made, and the "other" field typed in if the choice
    made was the last one.
    """
    
    def __init__(self, *args, **kwargs):
        fields = [
            forms.ChoiceField(widget=forms.RadioSelect(renderer=RadioSelectWithOtherRenderer), *args, **kwargs),
            forms.CharField(required=False)
        ]
        widget = RadioSelectWithOtherWidget(choices=kwargs['choices'])
        kwargs.pop('choices')
        self._was_required = kwargs.pop('required', True)
        kwargs['required'] = False
        super(RadioSelectWithOtherField, self).__init__(widget=widget, fields=fields, *args, **kwargs)

    def compress(self, value):
        if self._was_required and not value or value[0] in (None, ''):
            raise forms.ValidationError(self.error_messages['required'])
        if not value:
            return [None, u'']
        return (value[0], value[1] if force_unicode(value[0]) == force_unicode(self.fields[0].choices[-1][0]) else u'')


class QuestionForm7(QuestionForm):
    
    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('choices')
        super(QuestionForm7, self).__init__(*args, **kwargs)
        self.fields['choices'] = RadioSelectWithOtherField(choices=choices)
        
    def save(self, respondent):       
        try:
            super(QuestionForm7, self).save(respondent)
            question_answer = QuestionAnswer.objects.get(pk=self.cleaned_data['choices'][0])
            respondent_answer = RespondentAnswer(respondent=respondent, question=self.question, choice=question_answer, text=self.cleaned_data['choices'][1])
            respondent_answer.save()
        except:
            print 'Wystapił błąd podczas zapisywania formularza 7'
    
    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += '%s' % self['choices'].errors
        output += '%s' % self['choices']
        return mark_safe(output)
    
# ---------------------------------------------------------------------
# --- PYTANIE WIELOKROTNEGO WYBORU Z POLEM INNE
# ---------------------------------------------------------------------    


class CheckboxSelectMultipleWithOther(forms.CheckboxSelectMultiple):
                
    def render(self, name, value, attrs=None, choices=()):
        # self.choices przechowuje wszystkie elementy wyboru poza ostatnim
        # self.other przechowuje ostatni element wyboru
        self.choices, self.other = self.choices[:-1], self.choices[-1]
        
        if value is None: value = []
        has_id = attrs and 'id' in attrs
        final_attrs = self.build_attrs(attrs, name=name)
        output = [u'<ul>']
        # Normalize to strings
        str_values = set([force_unicode(v) for v in value])
        for i, (option_value, option_label) in enumerate(chain(self.choices, choices)):
            # If an ID attribute was given, add a numeric index as a suffix,
            # so that the checkboxes don't all have the same ID attribute.
            if has_id:
                final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))
                label_for = u' for="%s"' % final_attrs['id']
            else:
                label_for = ''

            cb = CheckboxInput(final_attrs, check_test=lambda value: value in str_values)
            option_value = force_unicode(option_value)
            rendered_cb = cb.render(name, option_value)
            option_label = conditional_escape(force_unicode(option_label))
            output.append(u'<li><label%s>%s %s </label></li>' % (label_for, rendered_cb, option_label))
        
        # Wygenerowanie ostatniego elementu wyboru wraz z polem tekstowym
        # self.other[0] zawiera wartości elementu formularza
        # self.other[1] zawiera etykietę elementu formularza

        other_value = self.other[0]
        other_label = self.other[1]
               
        if has_id:
            other_checkbox_id = len(self.choices)+1
            final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], other_checkbox_id))
            label_for = u' for="%s"' % final_attrs['id']
        else:
            label_for = ''
                
        cb = CheckboxInput(final_attrs, check_test=lambda value: value in str_values)
        option_value = force_unicode(other_value)
        rendered_cb = cb.render(name, option_value)
        option_label = conditional_escape(force_unicode(other_label))
        output.append(u'<li><label class="other">%s %s</label>%%s</li>' % (rendered_cb, other_label))
        
        output.append(u'</ul>')
        return mark_safe(u'\n'.join(output))


class MultipleChoiceWithOtherWidget(forms.MultiWidget):

    def __init__(self, choices):
        widgets = [
            CheckboxSelectMultipleWithOther(choices=choices),
            forms.TextInput
        ]
        super(MultipleChoiceWithOtherWidget, self).__init__(widgets)

    def decompress(self, value):
        """
        Rozdziela skompresowaną wartość na poszczególne pola.
        W tym przypadku value powinno być tuplą gdzie pierwszy element to wartość dla radio buttona a druga dla pola tekstowego.
        """
        if not value:
            return [None, None]
        return value

    def format_output(self, rendered_widgets):
        """Format the output by substituting the "other" choice into the first widget."""
        return rendered_widgets[0] % rendered_widgets[1]


class MultipleChoiceWithOtherField(forms.MultiValueField):
    """
    W klasie dziedziczącej po MultiValueField należy zaimplementować metodę compress. 
    """
       
    def __init__(self, *args, **kwargs):
        fields = [
            forms.MultipleChoiceField(widget=CheckboxSelectMultipleWithOther, *args, **kwargs),
            forms.CharField(required=False)
        ]
        widget = MultipleChoiceWithOtherWidget(choices=kwargs['choices'])
        kwargs.pop('choices')
        self._was_required = kwargs.pop('required', True)
        kwargs['required'] = False
        super(MultipleChoiceWithOtherField, self).__init__(widget=widget, fields=fields, *args, **kwargs)

    def compress(self, value):
        """
        Otrzymuje zwalidowane odpowiedzi z pól składowych. Na wyjściu powinna pojawić się odpo
        """
        if self._was_required and not value or value[0] in (None, ''): # Pole jest wymagane i nie została udzielona odpowiedź lub nie został zaznaczony żaden checkbox
            raise forms.ValidationError(self.error_messages['required'])
        
        if not value: # Pole nie jest wymagane i nie została wybrana żadna odpowiedź
            return [None, u'']
        
        return (value[0], value[1] if force_unicode(value[0]) == force_unicode(self.fields[0].choices[-1][0]) else u'')


class QuestionForm8(QuestionForm):
    
    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('choices')
        super(QuestionForm8, self).__init__(*args, **kwargs)
        self.fields['choices'] = MultipleChoiceWithOtherField(choices=choices)
        
    def save(self, respondent):       
        try:
            super(QuestionForm8, self).save(respondent)
            for id_question_answer, answer_text in self.cleaned_data['choices']:
                question_answer = QuestionAnswer.objects.get(pk=id_question_answer)
                respondent_answer = RespondentAnswer(respondent=respondent, question=self.question, choice=question_answer, text=answer_text)
                respondent_answer.save()
        except:
            print 'Wystapił błąd podczas zapisywania formularza 8'
    
    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += '%s' % self['choices'].errors
        output += '%s' % self['choices']
        return mark_safe(output)
    
# ---------------------------------------------------------------------
# --- PYTANIE DATOWE
# ---------------------------------------------------------------------


class QuestionForm9(QuestionForm):
    """
    Pytanie datowe.
    """
    def __init__(self, *args, **kwargs):
        super(QuestionForm9, self).__init__(*args, **kwargs)

        # teraz
        now = datetime.datetime.now()
        # lata
        years = []
        for year in range(now.year, 1910, -1):
            years.append([year, year])
        # miesiace
        months = [ [1, 'styczen'], [2, 'luty'], [3, 'marzec'],
                   [4, 'kwiecien'], [5, 'maj'], [6, 'czerwiec'],
                   [7, 'lipiec'], [8, 'sierpien'], [9, 'wrzesien'],
                   [10, 'pazdziernik'], [11, 'listopad'], [12, 'grudzien'] ]
        # dni
        days = []
        for day in range(1, 32):
            days.append([day, u'%02i' % day])
            
        self.fields['year'] = forms.IntegerField(widget=forms.Select(choices=years), initial=now.year, validators = [MinValueValidator(1910), MaxValueValidator(now.year)])
        self.fields['month'] = forms.IntegerField(widget=forms.Select(choices=months), initial=now.month, validators = [MinValueValidator(1), MaxValueValidator(12)])
        self.fields['day'] = forms.IntegerField(widget=forms.Select(choices=days), initial=now.day, validators = [MinValueValidator(1), MaxValueValidator(31)])

    def save(self, respondent):
        super(QuestionForm9, self).save(respondent)
        respondent_answer = RespondentAnswer(respondent=respondent, question=self.question, text=datetime.datetime.strptime('%d-%d-%d' % (self.cleaned_data['year'], self.cleaned_data['month'], self.cleaned_data['day']), '%Y-%m-%d') )
        respondent_answer.save()

    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += '%s' % self['year'].errors
        output += '%s' % self['year']
        output += '%s' % self['month'].errors
        output += '%s' % self['month']
        output += '%s' % self['day'].errors
        output += '%s' % self['day']
        return mark_safe(output)


class QuestionForm10(QuestionForm):
    """
    Pytanie datowe.
    """
    def __init__(self, *args, **kwargs):
        super(QuestionForm10, self).__init__(*args, **kwargs)
        
        # teraz
        now = datetime.datetime.now()
        # lata
        years = []
        for year in range(now.year, 1910, -1):
            years.append([year, year])
        # miesiace
        months = [ [1, 'styczen'], [2, 'luty'], [3, 'marzec'],
                   [4, 'kwiecien'], [5, 'maj'], [6, 'czerwiec'],
                   [7, 'lipiec'], [8, 'sierpien'], [9, 'wrzesien'],
                   [10, 'pazdziernik'], [11, 'listopad'], [12, 'grudzien'] ]

        self.fields['year'] = forms.IntegerField(widget=forms.Select(choices=years), initial=now.year, validators=[MinValueValidator(1910), MaxValueValidator(now.year)])
        self.fields['month'] = forms.IntegerField(widget=forms.Select(choices=months), initial=now.month, validators=[MinValueValidator(1), MaxValueValidator(12)])

    def save(self, respondent):
        super(QuestionForm10, self).save(respondent)
        respondent_answer = RespondentAnswer(respondent = respondent, question = self.question, text = datetime.datetime.strptime('%d-%d-%d' % (self.cleaned_data['year'], self.cleaned_data['month'], 1), '%Y-%m-%d') )
        respondent_answer.save()

    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += '%s' % self['year'].errors
        output += '%s' % self['year']
        output += '%s' % self['month'].errors
        output += '%s' % self['month']
        return mark_safe(output)


class QuestionForm11(QuestionForm):
    """
    Pytanie datowe.
    """
    def __init__(self, *args, **kwargs):
        super(QuestionForm11, self).__init__(*args, **kwargs)
        
        # teraz
        now = datetime.datetime.now()
        # lata
        years = []
        for year in range(now.year, 1910, -1):
            years.append([year, year])

        self.fields['year'] = forms.IntegerField(widget=forms.Select(choices=years), initial=now.year, validators=[MinValueValidator(1910), MaxValueValidator(now.year)])

    def save(self, respondent):
        super(QuestionForm11, self).save(respondent)
        respondent_answer = RespondentAnswer(respondent=respondent, question=self.question, text=datetime.datetime.strptime('%d-%d-%d' % (self.cleaned_data['year'], 1, 1), '%Y-%m-%d') )
        respondent_answer.save()

    def __unicode__(self):
        output = u'<p class="question">%s</p>' % self.question
        output += '%s' % self['year'].errors
        output += '%s' % self['year']
        return mark_safe(output)


# ---------------------------------------------------------------------
# --- 
# ---------------------------------------------------------------------

class TokenForm(forms.Form):
    token = forms.CharField(max_length=16, label="Token")
