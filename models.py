# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

# -----------------------------------------------
# --- STRUKTURA
# -----------------------------------------------


class Poll(models.Model):
    class Meta:
        db_table = 'orb_poll'
        verbose_name = _(u'Poll')
        verbose_name_plural = _(u'Polls')
    
    title = models.CharField(max_length=256, verbose_name=_("Title"))
    date_add = models.DateTimeField(auto_now_add=True, verbose_name=_("Creation date"))
    date_from = models.DateField(null=True, blank=True, verbose_name=_("Date from"))
    date_to = models.DateField(null=True, blank=True, verbose_name=_("Date to"))
    poll_file = models.FileField(upload_to='orb/poll', verbose_name=_("Poll file (XML)"), null=True, blank=True)
    respondents_file = models.FileField(upload_to='orb/poll', verbose_name=_("Respondents file (CSV)"), null=True, blank=True)
    announcement_title = models.CharField(null=True, blank=True, max_length=256, verbose_name="Tytuł ogłoszenia")
    announcement_content = models.TextField(null=True, blank=True, verbose_name=_("Announcement content"))
    invitation_title = models.CharField(null=True, blank=True, max_length=256, verbose_name="Tytuł zaproszenia")
    invitation_content = models.TextField(null=True, blank=True, verbose_name=_("Invitation content"), help_text=_(u"Type %url% to add personal poll url"))
    reminder_title = models.CharField(null=True, blank=True, max_length=256, verbose_name="Tytuł przypomnienia")
    reminder_content = models.TextField(null=True, blank=True, verbose_name="Treść przypomnienia")
    thanks_content = models.TextField(null=True, blank=True, verbose_name="Treść podziękowania", help_text=_(u"Type %token% to display token and %vcode% to display verification code"))
    
    def __unicode__(self):
        return "%s (%s - %s)" % (unicode(self.title), unicode(self.date_from), unicode(self.date_to))


class PollType(models.Model):
    class Meta:
        db_table = 'orb_poll_type'
        verbose_name = _(u'Poll type')
        verbose_name_plural = _(u'Poll types')
    
    name = models.CharField(max_length=256)
    
    def __unicode__(self):
        return unicode(self.name)


class ActivePollType(models.Model):
    class Meta:
        db_table = 'orb_active_poll_type'
        verbose_name = _(u'Active poll type')
        verbose_name_plural = _(u'Active poll types')
        
    poll = models.OneToOneField('Poll')
    type = models.OneToOneField('PollType')
    
    def __unicode__(self):
        return "%s: %s" % (unicode(self.type), unicode(self.poll))


class Stage(models.Model):
    class Meta:
        db_table = 'orb_stage'
        verbose_name = _(u'Stage')
        verbose_name_plural = _(u'Stages')
        ordering = ('order',)
        
    poll = models.ForeignKey('Poll')
    order = models.SmallIntegerField()

    def __unicode__(self):
        return "%s" % unicode(self.order)


class Sheet(models.Model):
    class Meta:
        db_table = 'orb_sheet'
        verbose_name = _(u'Sheet')
        verbose_name_plural = _(u'Sheets')
        ordering = ('stage__order', 'order')
        
    node_id = models.SmallIntegerField()
    stage = models.ForeignKey('Stage')
    title = models.CharField(max_length=256)
    info = models.TextField()
    order = models.SmallIntegerField()
    
    def __unicode__(self):
        return self.title
    
    def get_conditions(self):
        """
        Zwraca warunki, których spełnienie powoduje wyświetlenie tego arkusza.
        """
        return self.condition_set.all()
    
    def is_filled(self, respondent):
        """
        Zwraca True jeżeli respondent zatwierdził arkusz. 
        """
        return True if self in respondent.sheets_completed.all() else False


class Question(models.Model):
    class Meta:
        db_table = 'orb_question'
        verbose_name = _(u'Question')
        verbose_name_plural = _(u'Questions')
        ordering = ('order',)
        
    node_id = models.SmallIntegerField()
    sheet = models.ForeignKey('Sheet')
    content = models.TextField()
    type = models.SmallIntegerField()
    max_answers = models.SmallIntegerField(null=True, blank=True)
    order = models.SmallIntegerField(null=True, blank=True)
    
    def __unicode__(self):
        return self.content.replace('\n', '<br/>')


class SubQuestion(models.Model):
    class Meta:
        db_table = 'orb_subquestion'
        verbose_name = _(u'Subquestion')
        verbose_name_plural = _(u'Subquestions')
        ordering = ('order',)
        
    node_id = models.SmallIntegerField()
    content = models.TextField()
    question = models.ForeignKey('Question')
    order = models.SmallIntegerField(null=True, blank=True)
    
    def __unicode__(self):
        return self.content


class QuestionAnswer(models.Model):
    class Meta:
        db_table = 'orb_question_answer'
        verbose_name = _(u'Question answer')
        verbose_name_plural = _(u'Question answers')
        ordering = ('order',)
        
    node_id = models.SmallIntegerField()
    question = models.ForeignKey('Question')
    content = models.TextField()
    order = models.SmallIntegerField(null=True, blank=True)
    
    def __unicode__(self):
        return self.content
    
# -----------------------------------------------
# --- CONDITIONS
# -----------------------------------------------


class Condition(models.Model):
    class Meta:
        db_table = 'orb_condition'
        verbose_name = _(u'Condition')
        verbose_name_plural = _(u'Conditions')

    poll = models.ForeignKey('Poll')
    name = models.CharField(max_length=256, null=True, blank=True)
    condition_components = models.ManyToManyField('ConditionComponent', db_table="orb_condition__to__condition_component")
    sheets = models.ManyToManyField('Sheet', db_table='orb_condition__to__sheet')

    def __unicode__(self):
        return "%s" % self.name


class ConditionComponent(models.Model):
    class Meta:
        db_table = 'orb_condition_component'
        verbose_name = _(u'Condition component')
        verbose_name_plural = _(u'Condition components')

    sheet = models.ForeignKey('Sheet')
    question = models.ForeignKey('Question')
    answer = models.ForeignKey('QuestionAnswer')
    negation = models.BooleanField(default=False)

    def __unicode__(self):
        return '"%s": "%s"' % (unicode(self.question), unicode(self.answer))

# -----------------------------------------------
# --- ANSWERS
# -----------------------------------------------


class Respondent(models.Model):
    class Meta:
        db_table = 'orb_respondent'
        verbose_name = _(u'Respondent')
        verbose_name_plural = _(u'Respondents')
        
    poll = models.ForeignKey('Poll')
    email = models.EmailField()
    token = models.CharField(max_length=16)
    vcode = models.CharField(max_length=16)
    sheets_completed = models.ManyToManyField('Sheet', null=True, blank=True, db_table='orb_respondent__to__sheet')
    is_announcement_sent = models.BooleanField(default=False)
    is_invitation_sent = models.BooleanField(default=False)
    is_reminder_sent = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    last_update = models.DateField(null=True, blank=True)

    def has_answer(self, answer, negation):
        result = True if RespondentAnswer.objects.filter(respondent=self, choice=answer).count() else False
        if negation:
            result = not result
        return result
    
    def __unicode__(self):
        return self.email


class RespondentAnswer(models.Model):
    class Meta:
        db_table = 'orb_respondent_answer'
        verbose_name = _(u'Respondent answer')
        verbose_name_plural = _(u'Respondent answers')
        
    respondent = models.ForeignKey('Respondent')
    choice = models.ForeignKey('QuestionAnswer', null=True, blank=True)
    text = models.TextField(null=True, blank=True)
    question = models.ForeignKey('Question')
    subquestion = models.ForeignKey('Subquestion', null=True, blank=True)